SHELL := /bin/bash
PROJECT_DIR=/home/roman/projects/pyramid/pggeo
VENV_DIR = /home/roman/.virtualenvs
CURR_ENV_NAME = pyramid34

runtests:
	python $(PROJECT_DIR)/setup.py test --test-suite pggeo.tests

runW:
	pserve development.ini --reload

runG:
	gunicorn --paste development.ini --reload
