# -*- coding: utf-8 -*-

"""
Put __doc__ info here.
"""
import re
import json
# import lxml
from urllib import request
from urllib import parse


from . import settings

class PyramidGoogleGeocoder(object):
    """
    Основний клас, що відповідає за отримання від сервісу Google Maps
    координат або адрес.
    """
    geo_url = settings.GEO_URL
    geo_request_url = None
    api_key = settings.API_KEY
    outp_type = settings.OUTP_TYPE
    address_qstring = settings.ADDRESS_QSTRING
    coords_qstring = settings.COORDS_QSTRING
    apikey_qstring = settings.APIKEY_QSTRING
    lang_qstring = settings.LANG_QSTRING
    lang = settings.LANG
    request_address = None
    request_address_string = None
    request_coords = None
    request_coords_string = None
    raw_data = None
    processed_data = None
    # setting proxy section
    proxy_address = None
    proxy = None

    def __init__(self, request_address=None, request_coords=None, proxy_address=None):
        """
        Під час створення нового об’єкту геокодера отримується параметр
        request_address/адреса запиту (необ.) або 
        request_coords/координати запиту (необ.)
        
        Залежно від того, отримано адресу чи координати, в ініті відпрацьовує
        метод підготовки рядка запиту з адресою або з координатами.
        
        У фіналі ініта спрацьовує метод, що формує з даних, 
        котрі містяться в конфігу і з підготованого рядка запиту повний урл
        для спрямування запиту у Google Geocoder.
        
        Метод get_data запускається окремо в контексті виконання скрипта і 
        отримує дані із підготовленого урла. Залежно від параметрів 
        конфігурації дані приходять як json або xml.
        
        За надання адаптованих до використання даних відповідають методи
        process_json & process_xml
        """
        self.request_address = parse.quote(request_address)
        self.request_coords = request_coords
        self.proxy_address = proxy_address
        # "сирі" дані запиту до ресурсу
        self.raw_data = None
        # дані, підготовані до обробки з урахуванням self.outp_type
        self.prepared_data = None
        # готові до виводу дані
        self.processed_data = None
        
        if self.request_address:
            self.request_coords = None
            self._process_request_address_string()

        if self.request_coords:
            self.request_address = None
            self._process_request_coords_string()

        if self.proxy_address:
            self.set_proxy_handler()
            
        self._process_geo_request_url()

    def _process_request_address_string(self):
        """
        Підготовка рядка з адресою для запиту.
        Результат роботи -  властивість self.request_address_string, котра
        використовується у методі self._process_geo_request_url(генерування
        повного рядка запиту до геокодера)
        sub1 - видаляємо ліві розділові знаки, замінюючи їх пробілами
        sub2 - видаляємо зайві пробіли
        """
        if self.request_address:
            sub1 = re.sub('[\.,;\:]+',' ', self.request_address, flags=re.I+re.U)
            sub2 = re.sub('[\s]{2,}',' ', sub1, flags=re.I+re.U)
            self.request_address_string = \
            '+'.join(sub2.split(' '))

    def _process_request_coords_string(self):
        """
        Підготовка рядка з координатами для запиту.
        Результат роботи -  властивість self.request_coords_string, котра
        використовується у методі self._process_geo_request_url(генерування
        повного рядка запиту до геокодера)
        sub1 - видаляємо ліві розділові знаки, замінюючи їх пробілами
        sub2 - видаляємо зайві пробіли
        
        """
        if self.request_coords:
            if isinstance(self.request_coords, (list,tuple)):
                self.request_coords_string = ','.join([str(i) for i in self.request_coords])
            elif isinstance(self.request_coords, (str,)):
                sub1 = re.sub('[,;\:]+',' ', self.request_coords)
                sub2 = re.sub('[\s]{2,}',' ', sub1)
                self.request_coords_string = ','.join(sub2.split(' '))

    def _process_geo_request_url(self):
        """
        Prepare geo_request_url for
        """
        self.geo_request_url = self.geo_url
        if self.request_address_string:
            self.geo_request_url = \
            ''.join([
                    self.geo_request_url,
                    self.outp_type,
                    '?',
                    self.address_qstring.format(self.request_address_string),
                    '&' + self.apikey_qstring.format(self.api_key) if self.api_key else '',
                    ])
        elif self.request_coords_string:
            self.geo_request_url = \
            ''.join([
                    self.geo_request_url,
                    self.outp_type,
                    '?',
                    self.coords_qstring.format(self.request_coords_string),
                    '&' + self.apikey_qstring.format(self.api_key) if self.api_key else '',
                    ])
        if self.lang:
            self.geo_request_url = \
            self.geo_request_url + '&' + self.lang_qstring.format(self.lang)
            
    def process_json(self, **kw):
        """ Обробка отриманих даних, конвертування у json """
        data = str(self.raw_data.read().decode('UTF-8'))
        self.prepared_data = json.loads(data)
        self.processed_data = self.prepared_data
        
    def process_xml(self, **kw):
        pass

    def get_data(self, **kw):
        """
        Запит даних про об’єкт через Google API.
        Залежно від значення self.outp_type відпрацьовує потрібний метод, що
        проводить коректну підготовку даних до подальшого опрацювання.
        kw призначений для параметрів, котрі регулюватимуть подальшу обробку даних.
        Наприклад,kw['location'] = True викличе додатковий метод геолокатора
        get_location, котрий видасть тільки координати запитаної точки,
        get_location_squared видасть координати, що сформують "квадрат"
        (якщо об’єкт геолокації може трактуватися як певна область на карті)
        """
        try:
            self.raw_data = request.urlopen(self.geo_request_url)
            getattr(self, 'process_{0}'.format(self.outp_type))(**kw)
        except AttributeError:
            pass

        if kw:
            """
            Якщо задано модифікатори, то намагаємося запустити відповідні
            методи. Помилки відпацьовувати в даному контексті не потрібно.
            1. Переформатовуємо self.processed_data у dict
            2. Запускаємо усі потрібні методи. Кожен метод додаватиме у
            self.processed_data власну секцію "імені себе"
            """
            self.processed_data = {}
            for k, v in kw.items():
                if v:
                    try:
                        getattr(self, 'get_{0}'.format(k))(k)
                    except:
                        pass
            if not self.processed_data:
                """
                Якщо жодної секції не було створено під час роботи 
                методів-модифікаторів(неіснуючі методи або помилки в їх роботі)
                то підготовані для обробки дані передаються у вивід без змін.
                """
                self.processed_data = self.prepared_data
        else:
            # немає модифікаторів - дані передаються у вивід без змін
            self.processed_data = self.prepared_data
        return self.processed_data

    def get_location(self, section_name):
        """
        Отримуємо координати об’єкта як точки.
        Для json і xml використовуються(природно) різні парадигми.
        Поки що хмл заглушений.
        """
        if self.outp_type == 'json':
            try:
                self.processed_data[section_name] = \
                self.prepared_data['results'][0]['geometry']['location']
            except:
                pass
        elif self.outp_type == 'xml':
            pass
    
    def get_location_squared(self, section_name):
        """
        Отримуємо координати об’єкта як області на карті.
        Для json і xml використовуються(природно) різні парадигми.
        Поки що хмл заглушений.
        """
        if self.outp_type == 'json':
            try:
                self.processed_data[section_name] = \
                self.prepared_data['results'][0]['geometry']['viewport']
            except:
                pass
        elif self.outp_type == 'xml':
            pass

    def set_proxy_handler(self):
        """
        Create proxy handler if needed
        """
        if isinstance(self.proxy_address, (dict,)):
            self.proxy = request.ProxyHandler(self.proxy_address)
            opener = request.build_opener(self.proxy)
            request.install_opener(opener)
