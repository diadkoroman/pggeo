# -*- coding: utf-8 -*-

"""
Put __doc__ info here.
"""
import json
import unittest
#from pyramid import testing


class PyramidGoogleGeocoderTests(unittest.TestCase):

    def test_testscondition(self):
        print('Tests are working!')

    def test_import_geocoder(self):
        try:
            from . import Geocoder
        except ImportError:
            print('Geocoder was not imported!')

    def test_create_geo_request_url_(self):
        """ Test for direct URL creation. """
        from . import Geocoder
        geo = Geocoder(request_address='київ, просп. леся курбаса, 4')
        print(geo.geo_request_url)

    def test_create_reverse_geo_request_url(self):
        """ Test for reverse URL creation """
        from . import Geocoder
        #geo = Geocoder(request_coords=(10.0001, 12.0002))
        #print(geo.geo_request_url)
        #geo = Geocoder(request_coords='10.0001,12.0002')
        #print(geo.geo_request_url)
        #geo = Geocoder(request_coords='10.0001;12.0002')
        #print(geo.geo_request_url)

    def test_get_data(self):
        try:
            from urllib import request
        except ImportError:
            print('Import "request" module failed')
            return False
        from . import Geocoder
        try:
            geo = Geocoder(request_address='київ, просп. леся курбаса, 4')
            geo.get_data()
            print(geo.processed_data)
        except:
            geo = Geocoder(
                request_address='київ, просп. леся курбаса, 4',
                proxy_address=
                {'http':'192.168.50.9:8080',
                'https':'192.168.50.9:8080'})
            geo.get_data()
            print(geo.processed_data)
        #except:
        #    print('Getting data failed')
    '''
    def test_get_location(self):
        """ Тестуємо можливість отримання координат точки """
        try:
            from urllib import request
        except ImportError:
            print('Import "request" module failed')
            return False
        from . import Geocoder
        #try:
        geo = Geocoder(request_address='київ, просп. леся курбаса, 4')
        geo.get_data(location=True)
        print(geo.processed_data)
        #except:
        #    print('Getting location failed')
            
    def test_get_location_squared(self):
        """ Тестуємо можливість отримання координат області """
        try:
            from urllib import request
        except ImportError:
            print('Import "request" module failed')
            return False
        from . import Geocoder
        try:
            geo = Geocoder(request_address='київ, просп. леся курбаса, 4')
            geo.get_data(location_squared=True)
            print(geo.processed_data)
        except:
            print('Getting location_squared failed')
    '''
