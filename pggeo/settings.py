# -*- coding: utf-8 -*-

"""
Put __doc__ info here.
"""

""" Base URL to request proper Google API """
GEO_URL = 'https://maps.googleapis.com/maps/api/geocode/'

""" Set your server`s API key. Additional info at:  """
API_KEY = 'AIzaSyA8xg19p-hCZ34TwD8kEABBUr5YoQN17eE'

"""
Output type for Google API responses. There are two options: 
'json' or 'xml'. Default is json. Why? I don`t know...:)
"""
OUTP_TYPE = 'json'

"""
URL query string for requesting coordinates by address (direct geocoding)
"""
ADDRESS_QSTRING = 'address={0}'

"""
URL query string for requesting address by coordinates (reverse geocoding)
"""
COORDS_QSTRING = 'latlng={0}'

"""
URL query string for api key
"""
APIKEY_QSTRING = 'key={0}'

LANG_QSTRING = 'language={0}'

"""
'language' query string for reverse geocoding. You need set this value if you
suppose using reverse queries in your code.
Info about supported values at: 
https://developers.google.com/maps/faq#languagesupport
"""
LANG = 'uk'
